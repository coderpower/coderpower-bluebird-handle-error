var Promise = require("bluebird");

module.exports = function handleError() {

    function resolvedPromise(counter) {
        return new Promise(function(resolve) {
            resolve(counter + 1);
        });
    }

    function rejectedPromise(counter) {
        return new Promise(function(resolve, reject) {
            reject('A promise failed after ' + counter + ' resolved promises');
        });
    }

    return resolvedPromise(0)
    .then(resolvedPromise)
    .then(rejectedPromise)
    .then(resolvedPromise)
    .catch(function(error) {
        console.log('Error:' + error);
    });

};