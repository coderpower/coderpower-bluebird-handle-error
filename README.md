### Handle Error

In this video, you’ll learn how to handle errors while chaining promises.
First, we will write a function that returns a resolved Promise:

```javascript
    function resolvedPromise(counter) {
        return new Promise(function(resolve){
            resolve(counter+1);
        });
    }
```

It takes as parameter a number that will be passed incremented by one along resolved callback.
Let’s write another one that throws an error containing the number which will be passed by previous

resolvedPromise calls:

```javascript
    function rejectedPromise(counter) {
        return new Promise(function(resolve, reject){
            reject('A promise failed after '+counter+' resolved promises');
        });
    }
```

We will now begin chaining those calls, initializing the number of resolved promises with 0:

```javascript
     resolvedPromise(0)
     .then(resolvedPromise)
     .then(rejectedPromise)
     .then(resolvedPromise)
     .catch(function(error){
         console.log('Error:'+error);
     });
```
What happens there ?

* The first resolvedPromise call takes 0 as parameter, and returns 1

* The second calls takes 1, and returns 2

* The next `then` calls rejectedPromise, which will throw an error containing the number of

resolvedPromise calls, which will be 2

* All next `then`statement are therefore skipped, thus not incrementing our value

* `catch` is finally called, displaying our message : “A promise failed after 2 resolved promises”

We can chain as much statement as we want after our rejectedPromise, they won’t be called because our error will directly be propagated to our chain’s error handler.
But all other promises before the rejected one will be executed.
In following warmup, be ready to code your first error handler!  


For more information, please refer to the documentation: http://bluebirdjs.com/docs/api-reference.html
